package eleks.com

import java.util.Properties

import eleks.com.Serializers.AvroSerde
import org.apache.kafka.streams.StreamsConfig

object Main {
  def main(args: Array[String]): Unit = {
    val properties = configureStreamerProps()
    CountriesPostStatistics.configure(properties)

    var countiesPostsCountThread = new Thread(CountriesPostStatistics)
    countiesPostsCountThread.run()

  }
  private def configureStreamerProps(): Properties ={
    val streamProps = new Properties()
    streamProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "CountriesPostsCount")
    streamProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,  "localhost:9092")
    streamProps.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, classOf[AvroSerde])
    streamProps.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,  classOf[AvroSerde])

    streamProps
  }
}
