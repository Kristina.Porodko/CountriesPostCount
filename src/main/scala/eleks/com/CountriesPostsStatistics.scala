package eleks.com

import Entities._
import Serializers.JSONSerde
import Entities.CountryPostCount
import com.sksamuel.avro4s.RecordFormat
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.streams.{KafkaStreams, KeyValue}
import org.apache.kafka.streams.kstream._

import scala.collection.mutable
import java.lang.Long
import java.util.Properties
import java.util.concurrent.TimeUnit

object CountriesPostStatistics extends Runnable{
  val userFormatter: RecordFormat[User] = RecordFormat[User]
  val postFormatter: RecordFormat[Post] = RecordFormat[Post]
  val countryPostCountFormatter: RecordFormat[CountryPostCount] = RecordFormat[CountryPostCount]
  val userIdPostCountFormatter: RecordFormat[UserIdPostCount] = RecordFormat[UserIdPostCount]

  var streams: KafkaStreams = null
  val streamBuilder: KStreamBuilder = new KStreamBuilder()

  //  def transform(postsStream: KStream[Int, GenericRecord], usersTable: KTable[Int, GenericRecord]): KStream[String, GenericRecord] = {
  //    val userPostCountStream: KStream[Int, GenericRecord] = postsStream.map[Int, GenericRecord](new UserPostCountKeyValueMapper)
  //      .groupByKey()
  //      .count()//(TimeWindows.of(1000))
  //        .toStream()
  //      .map[Int, GenericRecord](new KeyValueMapper[Windowed[Int], Long, KeyValue[Int, GenericRecord]] {
  //      override def apply(key: Windowed[Int], value: Long): KeyValue[Int, GenericRecord] = {
  //        val newValue = UserIdPostCount(key.key(), value)
  //// !!!!!!
  //        new KeyValue(key.key(), userIdPostCountFormatter.to(newValue))
  //      }
  //    })

  def transform(postsStream: KStream[Int, GenericRecord], usersTable: KTable[Int, GenericRecord]): KStream[String, GenericRecord] = {
    val userPostCountStream: KStream[Int, Long] =
      postsStream.map[Int, GenericRecord](new UserPostCountKeyValueMapper)
        .groupByKey()
        .count((TimeWindows.of(1)))
        .toStream()
        .map[Int, Long](new KeyValueMapper[Windowed[Int], Long, KeyValue[Int, Long]] {
        override def apply(key: Windowed[Int], value: Long): KeyValue[Int, Long] = {
          new KeyValue[Int, Long](key.key(), value)
        }
      })


    // [KStream[UserId, CountryPostCount]]
    userPostCountStream.join[GenericRecord, GenericRecord](usersTable, new UserPostsCountValueJoiner() )
      .map[String, GenericRecord](new CountryPostCountKeyValueMapper)
      .groupByKey()
      .aggregate[mutable.Map[String, CountryPostCount]](
      new  CountryPostCountInitializer,
      new CountryPostCountAggregator,
      new JSONSerde[mutable.Map[String, CountryPostCount]]
    )
      .toStream()
      .map[String, GenericRecord](new CountryPostCountRecordMapper)
  }

  def configure(properties: Properties): Unit = {
    //TODO: take names of topics from some configs
    val usersTable = streamBuilder.table[Int, GenericRecord]("users")
    val postsStream: KStream[Int, GenericRecord] = streamBuilder.stream("posts")

    val outputStream: KStream[String, GenericRecord] = transform(postsStream, usersTable)
    outputStream.to("countriesPostCount")

    streams = new KafkaStreams(streamBuilder, properties)
  }


  class UserPostCountKeyValueMapper extends KeyValueMapper[Int, GenericRecord, KeyValue[Int, GenericRecord]] {
    override def apply(postId: Int, postRecord: GenericRecord) : KeyValue[Int, GenericRecord] = {
      val post = postFormatter.from(postRecord)
      val userId = post.userid
      new KeyValue(userId, postRecord)
    }
  }

  class UserPostsCountValueJoiner extends ValueJoiner[Long, GenericRecord, GenericRecord]{
    override def apply(postCount: Long, userRecord: GenericRecord): GenericRecord = {
      val user = userFormatter.from(userRecord)
      val country = user.country
      val countryPostCount = CountryPostCount(country, postCount)

      countryPostCountFormatter.to(countryPostCount)
    }
  }

  //after join with users -> key:Country value:postCount
  class CountryPostCountKeyValueMapper extends KeyValueMapper[Int, GenericRecord, KeyValue[String, GenericRecord]]{
    override def apply(key: Int, countryPostCountGenericRecord: GenericRecord): KeyValue[String, GenericRecord] = {
      val countryPostCount = countryPostCountFormatter.from(countryPostCountGenericRecord)
      val country = countryPostCount.country

      new KeyValue[String, GenericRecord](country, countryPostCountGenericRecord)
    }
  }


  // for aggregating
  class CountryPostCountInitializer extends Initializer[mutable.Map[String, CountryPostCount]]{
    override def apply(): mutable.Map[String, CountryPostCount] = {
      mutable.Map()
    }
  }

  class CountryPostCountAggregator extends Aggregator[String, GenericRecord, mutable.Map[String, CountryPostCount]]{
    override def apply(country: String, countryPostCountRecord: GenericRecord, aggregate: mutable.Map[String, CountryPostCount]): mutable.Map[String, CountryPostCount] = {
      val countryPostCount = countryPostCountFormatter.from(countryPostCountRecord)
      val postCount = countryPostCount.postCount

      if(aggregate.contains(country)){
        aggregate(country).postCount = aggregate(country).postCount + postCount
      } else{
        val countryPostCount = CountryPostCount(country, postCount)
        aggregate(country) = countryPostCount
      }
      aggregate
    }
  }

  class CountryPostCountRecordMapper extends KeyValueMapper[String, mutable.Map[String, CountryPostCount], KeyValue[String, GenericRecord]]{
    override def apply(country: String, countryPostCountMap: mutable.Map[String, CountryPostCount]): KeyValue[String, GenericRecord] = {
      val countryPostCountRecord = countryPostCountFormatter.to(countryPostCountMap(country))

      new KeyValue(country, countryPostCountRecord)
    }
  }

  override def run(){
    streams.start()

    Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
      override def run(): Unit = {
        streams.close(10, TimeUnit.SECONDS)
      }
    }))
  }

}

