package eleks.com.Entities

case class CountryPostCount(var country: String,
                            var postCount: Long) {
  def +(countryPostCount: CountryPostCount) : CountryPostCount ={
    CountryPostCount(country, this.postCount + postCount)
  }
}

